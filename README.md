# Operations on RF Images(summer internship)

* Bu çalışmayı 25 günlük yaz stajımda yaptım. Hocam tarafından verilen görevleri araştırma yaparak ve deneyerek sonuçlar elde etmeye çalıştım. Makine öğrenmesinde genel anlamda veride yapılan küçük değişiklikler bile bazen
sonuçlarda büyük değişiklikler yapabiliyor. Bu yüzden kod yazılırken farklı yöntemler denenmiş ve doğal olarak bu da kodun daha karmaşıklaşmasına neden olmuştur. Kodun geliştirilmeye ihtiyacı olduğundan aşağıda yapılacaklar listesi 
hazırlamış bulunmaktayım. Ders ve kulüp işlerinden zaman buldukça bu görevleri yapacağım.

##Yapılması Gerekenler

* Jupyter notebooktan python3 dosyasına geçirilmeli
* Yapısal olarak yazılmış olan kodların OOP prensiplerine göre modellenmesi
* Karmaşıklığı azaltmak adına koddaki temel yapı taşlarının modüllere bölünmesi(Modüler Programlama)
* Kullanılan datasetlrin sayısı ve kaynaklarıyla birlikte koyulması
* Hoca ile irtibata geçilerek TIPTEKNO19 için döküman hazırlanması

